package com.smile.ssm.service.impl;

import com.smile.ssm.entity.SysLogPO;
import com.smile.ssm.dao.SysLogDao;
import com.smile.ssm.service.SysLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统日志表 服务实现类
 *
 * @author Smile
 * @since 2021-11-29
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogDao, SysLogPO> implements SysLogService {

}
