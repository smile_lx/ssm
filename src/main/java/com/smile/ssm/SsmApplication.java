package com.smile.ssm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Smile
 */
@SpringBootApplication
@MapperScan("com.smile.ssm.dao")
@EnableScheduling
@EnableAsync
public class SsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsmApplication.class, args);
        System.out.println("******************SSM project start success ！！！***********************");
    }

}
