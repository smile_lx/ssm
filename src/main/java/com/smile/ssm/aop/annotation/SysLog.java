package com.smile.ssm.aop.annotation;

import java.lang.annotation.*;

/**
 * 自定义 log注解
 *
 * @author Smile
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {
    /**
     * 记录业务名称
     *
     * @return 业务名称
     */
    String value() default "";
}
