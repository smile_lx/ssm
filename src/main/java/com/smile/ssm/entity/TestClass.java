package com.smile.ssm.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author Michelle
 */
@Data
public class TestClass implements Serializable {
    /**
     * 项目名称
     */
    private String itemName;

    /**
     * 方案号
     */
    private String testNo;

    /**
     * 汇款人
     */
    private String sender;

    /**
     * 汇款金额
     */
    private String money;
    /**
     * 到账次数
     */
    private String times;

    /**
     * 承担科室
     */
    private String dutyDepartment;

    /**
     * 项目负责人
     */
    private String leader;

    /**
     * 科室经办人
     */
    private String departmentLeader;
    /**
     * 可变数据 科室名称对应的钱数
     */
    private HashMap<String, String> unKnowData;
}
