package com.smile.ssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.smile.ssm.dao.SysUserDao;
import com.smile.ssm.entity.SysUserPO;
import com.smile.ssm.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * 用户表 服务实现类
 *
 * @author Smile
 * @since 2021-11-24
 */
@Service
public  class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserPO> implements SysUserService {

}
