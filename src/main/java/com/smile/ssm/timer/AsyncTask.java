package com.smile.ssm.timer;

import com.smile.ssm.entity.SysUserPO;
import com.smile.ssm.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Smile
 */
@Component
@Slf4j
public class AsyncTask {
    @Resource
    private SysUserService service;

    @Async("asyncServiceExecutor")
    public void doTask1() throws InterruptedException {
        List<SysUserPO> list = service.list();
        System.out.println(list);
        log.info("Task1 started.");
        long start = System.currentTimeMillis();
        Thread.sleep(5000);
        long end = System.currentTimeMillis();

        log.info("Task1 finished, time elapsed: {} ms.", end - start);
    }
}
