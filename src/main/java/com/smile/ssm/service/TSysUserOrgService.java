package com.smile.ssm.service;

import com.smile.ssm.entity.TSysUserOrgPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 *  服务类
 *
 * @author Smile
 * @since 2021-11-24
 */
public interface TSysUserOrgService extends IService<TSysUserOrgPO> {

}
