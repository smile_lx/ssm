package com.smile.ssm.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * 用户角色关系表 API控制器
 *
 * @author Smile
 * @since 2021-11-24
 */
@RestController
@RequestMapping("/t-sys-user-role-po")
public class TSysUserRoleApi {

}
