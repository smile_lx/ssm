package com.smile.ssm.api;

import com.smile.ssm.entity.TestClass;
import com.smile.ssm.utils.HttpResponseUtil;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Smile
 */
@RestController
@RequestMapping("/test")
public class RealaicyTestApi {

    /**
     * 测试异步以及定时任务共存问题
     */
    @GetMapping("/poi")
    public void test(HttpServletResponse response, HttpServletRequest request) throws IOException {
        ArrayList<TestClass> testClasses = new ArrayList<>();
        //构造数据对象
        TestClass testClass = new TestClass();
        testClass.setItemName("测试项目1");
        TestClass testClass1 = new TestClass();
        testClass1.setItemName("测试项目2");
        HashMap<String, String> stringStringHashMap = new HashMap<>(2);
        stringStringHashMap.put("科室1", "122");
        stringStringHashMap.put("科室2", "12442");
        testClass.setUnKnowData(stringStringHashMap);
        testClasses.add(testClass);
        HashMap<String, String> stringHashMap = new HashMap<>(3);
        stringHashMap.put("科室1", "1322");
        stringHashMap.put("科室2", "132442");
        stringHashMap.put("科室3", "123442");
        testClass1.setUnKnowData(stringHashMap);
        testClasses.add(testClass1);

        extracted(request, response, testClasses);
    }

    private void extracted(HttpServletRequest request, HttpServletResponse response, ArrayList<TestClass> testClasses) throws IOException {
        Set<String> strings2 = new HashSet<>();
        for (TestClass aClass : testClasses) {
            strings2.addAll(aClass.getUnKnowData().keySet());
        }
        int rowNumber = 0;
        int cellNumber = 8;
        //2.读取excel模板,创建excel对象
        Workbook sheets = this.loadExcel("src/main/resources/测试表格.xlsx");
        //3.读取sheet对象
        Sheet sheet = sheets.getSheetAt(0);
        CellStyle cellStyle = sheets.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        //获取第一行的数据  进行单元格合并
        Row row = sheet.getRow(rowNumber);
        CellStyle cellStyle1 = row.getCell(0).getCellStyle();
        int allLength = cellNumber + strings2.size() - 1;
        CellRangeAddress region = new CellRangeAddress(rowNumber, rowNumber, cellNumber, allLength);
        sheet.addMergedRegion(region);
        Cell cell8 = row.getCell(cellNumber);
        cell8.setCellStyle(cellStyle1);
        //获取第二行的数据生成 动态列
        rowNumber++;
        Row row1 = sheet.getRow(rowNumber);
        for (String s : strings2) {
            Cell cell1 = row1.createCell(cellNumber);
            cell1.setCellStyle(cellStyle1);
            cell1.setCellValue(s);
            cellNumber++;
        }
        //处理第三行的数据
        for (TestClass aClass : testClasses) {
            rowNumber = rowNumber + 1;
            Row sheetRow = sheet.createRow(rowNumber);
            //这里直接赛数据吧
            Cell cell0 = sheetRow.createCell(0);
            cell0.setCellValue(aClass.getItemName());
            cell0.setCellStyle(cellStyle);

            Cell cell1 = sheetRow.createCell(1);
            cell1.setCellValue(aClass.getTestNo());
            cell1.setCellStyle(cellStyle);

            Cell cell2 = sheetRow.createCell(2);
            cell2.setCellValue(aClass.getSender());
            cell2.setCellStyle(cellStyle);

            Cell cell3 = sheetRow.createCell(3);
            cell3.setCellValue(aClass.getMoney());
            cell3.setCellStyle(cellStyle);

            Cell cell4 = sheetRow.createCell(4);
            cell4.setCellValue(aClass.getTimes());
            cell4.setCellStyle(cellStyle);

            Cell cell5 = sheetRow.createCell(5);
            cell5.setCellValue(aClass.getDutyDepartment());
            cell5.setCellStyle(cellStyle);

            Cell cell6 = sheetRow.createCell(6);
            cell6.setCellValue(aClass.getLeader());
            cell6.setCellStyle(cellStyle);

            Cell cell7 = sheetRow.createCell(7);
            cell7.setCellValue(aClass.getDepartmentLeader());
            cell7.setCellStyle(cellStyle);

            int i = 0;
            for (String s : strings2) {
                Cell rowCell = sheetRow.createCell(i + 8);
                rowCell.setCellValue(aClass.getUnKnowData().getOrDefault(s, "0.00"));
                rowCell.setCellStyle(cellStyle);
                i++;
            }
        }
        //将生成的数据写入文件缓存流之中
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        sheets.write(bos);
        //关闭两个流
        bos.close();
        sheets.close();
        //将生成的数据发送出去
        HttpResponseUtil.toWep(response, request, bos.toByteArray(), "test.xlsx");
    }

    public Workbook loadExcel(String filePath) throws IOException, EncryptedDocumentException {
        Workbook book = null;

        if (filePath == null) {
            return null;
        }

        if (filePath.endsWith(".xls")) {
            POIFSFileSystem ps = new POIFSFileSystem(new File(filePath));
            book = WorkbookFactory.create(ps);
        } else if (filePath.endsWith(".xlsx")) {
            InputStream input = new FileInputStream(filePath);
            book = new XSSFWorkbook(input);
        }
        return book;
    }


}
