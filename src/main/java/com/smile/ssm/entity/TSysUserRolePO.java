package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.smile.ssm.CommonPO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 用户角色关系表 实体
 *
 * @author Smile
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sys_user_role")
public class TSysUserRolePO extends CommonPO {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId("user_id")
    private Long userId;

    /**
     * 角色id
     */
    @TableField("role_id")
    private Long roleId;

    /**
     * 是否默认
     */
    @TableField("is_default")
    private Integer isDefault;


}
