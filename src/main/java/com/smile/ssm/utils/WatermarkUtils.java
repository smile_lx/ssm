package com.smile.ssm.utils;

import com.aspose.words.Shape;
import com.aspose.words.*;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.Font;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 文件添加水印相应工具类
 *
 * @author Smile
 */
@Slf4j
public class WatermarkUtils {
    /**
     * 为word文档添加水印
     *
     * @param doc           word文档模型
     * @param watermarkText 需要添加的水印字段
     * @throws Exception xx
     */
    public static void insertWatermarkText(Document doc, String watermarkText) throws Exception {
        Shape watermark = new Shape(doc, ShapeType.TEXT_PLAIN_TEXT);
        //水印内容
        watermark.getTextPath().setText(watermarkText);
        //水印字体
        watermark.getTextPath().setFontFamily("宋体");
        //水印宽度
        watermark.setWidth(300);
        //水印高度
        watermark.setHeight(20);
        //旋转水印
        watermark.setRotation(-40);
        //水印颜色 浅灰色
        watermark.getFill().setColor(Color.lightGray);
        watermark.setStrokeColor(Color.lightGray);
        //设置相对水平位置
        watermark.setRelativeHorizontalPosition(RelativeHorizontalPosition.PAGE);
        //设置相对垂直位置
        watermark.setRelativeVerticalPosition(RelativeVerticalPosition.PAGE);
        //设置包装类型
        watermark.setWrapType(WrapType.NONE);
        //设置垂直对齐
        watermark.setVerticalAlignment(VerticalAlignment.CENTER);
        //设置文本水平对齐方式
        watermark.setHorizontalAlignment(HorizontalAlignment.CENTER);
        Paragraph watermarkPara = new Paragraph(doc);
        watermarkPara.appendChild(watermark);
        for (Section sect : doc.getSections()) {
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_PRIMARY);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_FIRST);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_EVEN);
        }
        System.out.println("Watermark Set");
    }

    /**
     * 在页眉中插入水印
     *
     * @param watermarkPara 水印类型
     * @param sect          章节定位
     * @param headerType    位置定位
     */
    private static void insertWatermarkIntoHeader(Paragraph watermarkPara, Section sect, int headerType) {
        HeaderFooter header = sect.getHeadersFooters().getByHeaderFooterType(headerType);
        if (header == null) {
            header = new HeaderFooter(sect.getDocument(), headerType);
            sect.getHeadersFooters().add(header);
        }
        header.appendChild(watermarkPara.deepClone(true));
    }

    /**
     * 设置水印属性
     *
     * @param doc    需要添加水印的文档
     * @param wmText 水印字体
     * @param left   距离左边距离
     * @param top    距离顶部距离
     * @return 返回水印对象
     * @throws Exception xx
     */
    public static Shape shapeMore(Document doc, String wmText, double left, double top) throws Exception {
        //这里对数据进行处理
        String[] split = wmText.split("");
        String realWatermarkFont = String.join("  ", split);

        Shape waterShape = new Shape(doc, ShapeType.TEXT_PLAIN_TEXT);
        //水印内容
        waterShape.getTextPath().setText(realWatermarkFont);
        //水印字体
        waterShape.getTextPath().setFontFamily("宋体");
        waterShape.getTextPath().setSize(100);
        //水印宽度
        waterShape.setWidth(550);
        //水印高度
        waterShape.setHeight(100);
        //旋转水印
        waterShape.setRotation(-45);

        waterShape.setWrapType(0);
        waterShape.setBehindText(true);
        waterShape.setVerticalAlignment(0);
        //水印颜色 浅灰色
        /*waterShape.getFill().setColor(Color.lightGray);
        waterShape.setStrokeColor(Color.lightGray);*/
        waterShape.setStrokeColor(new Color(192, 192, 192));
        //将水印放置在页面中心
//        waterShape.setLeft(left);
        waterShape.setLeft(-50);
        waterShape.setTop(300);
        //设置包装类型
        waterShape.setWrapType(WrapType.NONE);
        return waterShape;
    }

    /**
     * 插入多个水印
     *
     * @param doc    需要添加水印的word 文档
     * @param wmText 字体水印数据
     * @throws Exception xx
     */
    public static void insertWaterMarkMore(Document doc, String wmText) throws Exception {
        Paragraph watermarkPara = new Paragraph(doc);
        //下面注释的是为了添加多个水印
        for (int j = 0; j < 500; j = j + 100) {
            for (int i = 0; i < 700; i = i + 85) {
                Shape waterShape = shapeMore(doc, wmText, j, j);
                watermarkPara.appendChild(waterShape);
            }
        }
        for (Section sect : doc.getSections()) {
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.FOOTER_FIRST);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_PRIMARY);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_FIRST);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_EVEN);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.FOOTER_EVEN);
        }
    }

    /**
     * 插入单个水印
     *
     * @param doc    需要添加水印的word 文档
     * @param wmText 字体水印数据
     * @throws Exception xx
     */
    public static void insertWaterMark(Document doc, String wmText) throws Exception {
        Paragraph watermarkPara = new Paragraph(doc);
        Shape waterShape1 = shapeMore(doc, wmText, 200, 300);
        watermarkPara.appendChild(waterShape1);
        for (Section sect : doc.getSections()) {
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.FOOTER_FIRST);
            insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HEADER_PRIMARY);
        }
    }

    /**
     * Embeds a textual watermark over a source image to produce
     * a watermarked one.
     *
     * @param text            The text to be embedded as watermark.
     * @param sourceImageFile The source image file.
     * @param destImageFile   The output image file.
     */
    public static void addTextWatermark(String text, File sourceImageFile, File destImageFile) {
        try {
            BufferedImage sourceImage = ImageIO.read(sourceImageFile);
            Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();

            // initializes necessary graphic properties
            AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f);
            g2d.setComposite(alphaChannel);
            g2d.setColor(Color.BLUE);
            g2d.setFont(new Font("Arial", Font.BOLD, 64));
            FontMetrics fontMetrics = g2d.getFontMetrics();
            Rectangle2D rect = fontMetrics.getStringBounds(text, g2d);

            // calculates the coordinate where the String is painted
            int centerX = (sourceImage.getWidth() - (int) rect.getWidth()) / 2;
            int centerY = sourceImage.getHeight() / 2;

            // paints the textual watermark
            g2d.drawString(text, centerX, centerY);

            ImageIO.write(sourceImage, "png", destImageFile);
            g2d.dispose();

            System.out.println("The tex watermark is added to the image.");

        } catch (IOException ex) {
            log.error(ex.toString());
        }
    }

    /**
     * Embeds an image watermark over a source image to produce
     * a watermarked one.
     *
     * @param watermarkImageFile The image file used as the watermark.
     * @param sourceImageFile    The source image file.
     * @param destImageFile      The output image file.
     */
    public static void addImageWatermark(File watermarkImageFile, File sourceImageFile, File destImageFile) {
        try {
            BufferedImage sourceImage = ImageIO.read(sourceImageFile);
            BufferedImage watermarkImage = ImageIO.read(watermarkImageFile);

            // initializes necessary graphic properties
            Graphics2D g2d = (Graphics2D) sourceImage.getGraphics();
            AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f);
            g2d.setComposite(alphaChannel);

            // calculates the coordinate where the image is painted
            int topLeftX = (sourceImage.getWidth() - watermarkImage.getWidth()) / 2;
            int topLeftY = (sourceImage.getHeight() - watermarkImage.getHeight()) / 2;

            // paints the image watermark
            g2d.drawImage(watermarkImage, topLeftX, topLeftY, null);

            ImageIO.write(sourceImage, "png", destImageFile);
            g2d.dispose();

            System.out.println("The image watermark is added to the image.");

        } catch (IOException ex) {
            log.error(ex.toString());
        }
    }


}
