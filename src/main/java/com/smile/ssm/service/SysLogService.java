package com.smile.ssm.service;

import com.smile.ssm.entity.SysLogPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 系统日志表 服务类
 *
 * @author Smile
 * @since 2021-11-29
 */
public interface SysLogService extends IService<SysLogPO> {

}
