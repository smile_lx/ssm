//package com.smile.ssm.filter;
//
//import com.smile.ssm.cons.HttpCons;
//import org.springframework.stereotype.Component;
//
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * @author Smile
// * @version v1.0
// * @description TODO
// * @time 2021/11/25 14:37
// */
//@Component
//public class WebFilter implements Filter {
//
//    @Override
//    public void init(FilterConfig filterConfig) {
//        //log.info("----------------过滤器初始化------------------------");
//    }
//
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        settCors(request, response);
//        //如果是OPTIONS请求就return 往后执行会到业务代码中 他不带参数会产生异常
//        if (request.getMethod().equals(HttpCons.OPTIONS)) {
//            filterChain.doFilter(servletRequest, servletResponse);
//            return;
//        }
//
//        //白名单不拦截
//        filterChain.doFilter(servletRequest, servletResponse);
//    }
//
//
//    @Override
//    public void destroy() {
//
//    }
//
//    private void settCors(HttpServletRequest request, HttpServletResponse response) {
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
//        response.setHeader("Access-Control-Max-Age", "3600");
//        response.setHeader("Access-Control-Allow-Headers", "Content-Type, x-requested-with, X-Custom-Header,at,tid");
//        //添加请求头 给前端获取文件流名称
//        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
//        response.setHeader("Access-Control-Allow-Credentials", "true");
//    }
//}
