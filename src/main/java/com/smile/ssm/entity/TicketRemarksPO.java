package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.smile.ssm.CommonPO;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 小票备注信息表 实体
 *
 * @author Smile
 * @since 2021-12-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("ticket_remarks")
public class TicketRemarksPO extends CommonPO {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 科室code
     */
    @TableField("code")
    private String code;

    /**
     * 科室名称
     */
    @TableField("name")
    private String name;

    /**
     * 温馨提示信息
     */
    @TableField("mark")
    private String mark;

    /**
     * 创建人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("oper_date")
    private LocalDateTime operDate;


}
