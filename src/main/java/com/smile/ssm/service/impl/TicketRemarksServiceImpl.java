package com.smile.ssm.service.impl;

import com.smile.ssm.entity.TicketRemarksPO;
import com.smile.ssm.dao.TicketRemarksDao;
import com.smile.ssm.service.TicketRemarksService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 小票备注信息表 服务实现类
 *
 * @author Smile
 * @since 2021-12-10
 */
@Service
public class TicketRemarksServiceImpl extends ServiceImpl<TicketRemarksDao, TicketRemarksPO> implements TicketRemarksService {

}
