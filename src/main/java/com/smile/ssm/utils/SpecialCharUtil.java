package com.smile.ssm.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @ClassName SpecialCharUtil
 * @Description: 处理特殊字符进行替换处理
 * @Author Smile
 * @Date 2021/1/5
 * @Version V1.0
 **/
public class SpecialCharUtil {
    /**
     * 校验是否存在特殊文件
     *
     * @param str 被校验的字符
     * @return 返回是否包含
     * @throws PatternSyntaxException xx
     */
    public static boolean checkSpecialChar(String str) throws PatternSyntaxException {
        // 清除掉所有特殊字符
        String regEx = ".*[`~!@#$%^&*()+=|{}':;,\\[\\].<>/?！￥…（）—【】‘；：”“’。，、？\\\\]+.*";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 校验字符如果存在进行 特殊字符就进行替换
     *
     * @param str        被校验的文件
     * @param replaceStr 替换的字符
     * @return 返回替换的字符
     * @throws PatternSyntaxException xx
     */
    public static String filterAndReplace(String str, String replaceStr) throws PatternSyntaxException {
        String regEx = ".*[`~!@#$%^&*()+=|{}':;,\\[\\].<>/?！￥…（）—【】‘；：”“’。，、？\\\\]+.*";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll(replaceStr).trim();
    }


}
