package com.smile.ssm.service;

import com.smile.ssm.entity.SysUserPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 用户表 服务类
 *
 * @author Smile
 * @since 2021-11-24
 */
public  interface SysUserService extends IService<SysUserPO> {

}
