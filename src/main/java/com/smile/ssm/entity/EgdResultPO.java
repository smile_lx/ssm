package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.smile.ssm.CommonPO;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 心电图检查结果信息表 实体
 *
 * @author Smile
 * @since 2021-12-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("egd_result")
public class EgdResultPO extends CommonPO {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 申请医生姓名
     */
    @TableField("doctor")
    private String doctor;

    /**
     * 申请医生code
     */
    @TableField("empl_code")
    private String emplCode;

    /**
     * 诊断
     */
    @TableField("diagnosis")
    private String diagnosis;

    /**
     * 审核人名称
     */
    @TableField("checker")
    private String checker;

    /**
     * 审核时间
     */
    @TableField("check_time")
    private LocalDateTime checkTime;

    /**
     * 执行时间
     */
    @TableField("execute_time")
    private LocalDateTime executeTime;

    /**
     * 患者姓名
     */
    @TableField("name")
    private String name;

    /**
     * 医嘱唯一编号
     */
    @TableField("order_id")
    private String orderId;


}
