package com.smile.ssm.service;

import com.smile.ssm.entity.EgdResultPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 心电图检查结果信息表 服务类
 *
 * @author Smile
 * @since 2021-12-21
 */
public interface EgdResultService extends IService<EgdResultPO> {

}
