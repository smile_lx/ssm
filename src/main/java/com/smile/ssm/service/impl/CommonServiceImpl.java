package com.smile.ssm.service.impl;

import com.smile.ssm.common.CommonDO;
import com.smile.ssm.dao.CommonDao;
import com.smile.ssm.service.CommonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统公用表 服务实现类
 *
 * @author Smile
 * @since 2022-03-24
 */
@Service
public class CommonServiceImpl extends ServiceImpl<CommonDao, CommonDO> implements CommonService {

}
