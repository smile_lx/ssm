package com.smile.ssm.dao;

import com.smile.ssm.entity.SysUserPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 用户表 Mapper 接口
 *
 * @author Smile
 * @since 2021-11-24
 */
public interface SysUserDao extends BaseMapper<SysUserPO> {

}
