package com.smile.ssm.api;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.smile.ssm.entity.EgdResultPO;
import com.smile.ssm.timer.AsyncTask;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 心电图检查结果信息表 API控制器
 *
 * @author Smile
 * @since 2021-12-21
 */
@RestController
@RequestMapping("/realTest")
public class EgdResultApi {
    @Resource
    private AsyncTask asyncTask;

    /**
     * 测试异步以及定时任务共存问题
     */
    @GetMapping("/test")
    public void test() {
        try {
            asyncTask.doTask1();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        EgdResultPO egdResultPO = new EgdResultPO();
        egdResultPO.setChecker("1111111'111111'33333");
        String s = JSON.toJSONString(egdResultPO);
        s = s.replace("'", "\\'");
        System.out.println(s);
            JSONObject jsonObject = JSON.parseObject(s);
        EgdResultPO egdResultPO1 = jsonObject.toJavaObject(EgdResultPO.class);
        System.out.println(egdResultPO1);
    }

}
