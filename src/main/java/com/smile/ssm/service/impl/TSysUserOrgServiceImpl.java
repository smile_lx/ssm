package com.smile.ssm.service.impl;

import com.smile.ssm.entity.TSysUserOrgPO;
import com.smile.ssm.dao.TSysUserOrgDao;
import com.smile.ssm.service.TSysUserOrgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 *  服务实现类
 *
 * @author Smile
 * @since 2021-11-24
 */
@Service
public class TSysUserOrgServiceImpl extends ServiceImpl<TSysUserOrgDao, TSysUserOrgPO> implements TSysUserOrgService {

}
