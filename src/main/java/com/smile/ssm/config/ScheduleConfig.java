//package com.smile.ssm.config;
//
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//
//import java.util.concurrent.Executors;
//import java.util.concurrent.ScheduledExecutorService;
//
///**
// * @ClassName ScheduleConfig
// * @Description: 定时类型配置项
// * @Author Smile
// * @Date 2020/11/9
// * @Version V1.0
// **/
//@Configuration
//public class ScheduleConfig implements SchedulingConfigurer {
//
//    @Override
//    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(4);
//        taskRegistrar.setScheduler(scheduledExecutorService);
//    }
//
//}
