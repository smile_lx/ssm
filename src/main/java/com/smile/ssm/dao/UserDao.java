package com.smile.ssm.dao;

import com.smile.ssm.entity.UserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 系统用户表 Mapper 接口
 *
 * @author Smile
 * @since 2022-03-24
 */
public interface UserDao extends BaseMapper<UserDO> {

}
