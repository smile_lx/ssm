package com.smile.ssm.aop.aspect;

import com.alibaba.fastjson.JSON;
import com.smile.ssm.aop.annotation.SysLog;
import com.smile.ssm.entity.SysLogPO;
import com.smile.ssm.service.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 * @author Smile
 * @version v1.0
 * @description
 * @time 2021/11/26 16:24
 */
@Aspect
@Component
@Slf4j
public class SysLogAspect {

    @Resource
    private SysLogService logService;
    private SysLogPO sysLogPO = new SysLogPO();

    /**
     * 这个是切点的意思，从什么地方切入： 可以是具体的类或者注解 ，也可以是模糊的类路径 * *..*Api.*(..)
     */
    @Pointcut("@annotation(com.smile.ssm.aop.annotation.SysLog)")
    public void annotationPointcut() {
    }

    /**
     * 前置通知, 在方法执行之前执行
     *
     * @param joinPoint xx
     */
    @Before("annotationPointcut()")
    public void beforePointcut(JoinPoint joinPoint) {
        System.out.println("beforePointcut");

    }

    /**
     * 环绕通知, 围绕着方法执行
     *
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("annotationPointcut()")
    public Object doAround(ProceedingJoinPoint point) throws Throwable {
        System.out.println("doAround");
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest request = sra.getRequest();
        //这个是获取调用的方法类型:get post ...
        String method1 = request.getMethod();
        String url = request.getRequestURL().toString();
        sysLogPO.setMethod(url);
        sysLogPO.setDeleted(false);
        sysLogPO.setCreateTime(LocalDateTime.now());
        //获得执行方法的类名
        String targetName = point.getTarget().getClass().getName();
        //获得执行方法的方法名
        String methodName = point.getSignature().getName();
        //获取切点方法的所有参数类型
        Object[] arguments = point.getArgs();
        try {
            Class targetClass = Class.forName(targetName);
            //获取公共方法，不包括类私有的
            Method[] methods = targetClass.getMethods();
            String value = "";
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                    //对比方法中参数的个数
                    Class[] clazzs = method.getParameterTypes();
                    if (clazzs.length == arguments.length) {
                        value = method.getAnnotation(SysLog.class).value();
                        break;
                    }
                }
            }
            sysLogPO.setBusinessName(value);
        } catch (Exception e) {
            e.printStackTrace();
        }

        logService.save(sysLogPO);
        log.info("===请求开始, 各个参数, url: " + url + ", method: " + method1 + ", uri:" + request.getRequestURI() + ", params:" + request.getQueryString());
        if (arguments != null) {
            for (Object argument : arguments) {
                log.info("===接口入参:" + JSON.toJSONString(argument));
            }
        }
        return point.proceed();

    }

    /**
     * 返回通知, 在方法返回结果之后执行
     *
     * @param joinPoint xx
     */
    @AfterReturning("annotationPointcut()")
    public void doAfterReturning(JoinPoint joinPoint) {
        System.out.println("doAfterReturning");
    }

    /**
     * 异常通知, 在方法抛出异常之后
     */
    @AfterThrowing("annotationPointcut()")
    public void doAfterThrowing(JoinPoint joinPoint) {
        System.out.println("doAfterThrowing");
    }

    /**
     * 后置通知, 在方法执行之后执行
     */
    @After("annotationPointcut()")
    public void doAfter(JoinPoint joinPoint) {
        System.out.println("doAfter");
    }


}
