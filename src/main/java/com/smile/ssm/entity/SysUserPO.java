package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.smile.ssm.CommonPO;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 用户表 实体
 *
 * @author Smile
 * @since 2021-11-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class SysUserPO extends CommonPO {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 用户真实姓名
     */
    @TableField("real_name")
    private String realName;

    /**
     * 移动电话
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 停启用状态（0：停用，1：启用）
     */
    @TableField("status")
    private Boolean status;

    /**
     * 逻辑删除标志（0：未删除，1：已删除）
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 登录默认机构
     */
    @TableField("default_org_id")
    private Long defaultOrgId;

    /**
     * 登录默认角色
     */
    @TableField("default_role_id")
    private Long defaultRoleId;

    /**
     * 创建人
     */
    @TableField("create_by")
    private Long createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private LocalDateTime deleteTime;

    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    /**
     * 最后登录IP
     */
    @TableField("last_login_ip")
    private String lastLoginIp;

    /**
     * 用户详情id
     */
    @TableField("sys_person_id")
    private Long sysPersonId;


}
