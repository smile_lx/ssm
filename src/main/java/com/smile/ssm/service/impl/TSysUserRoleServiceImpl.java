package com.smile.ssm.service.impl;

import com.smile.ssm.entity.TSysUserRolePO;
import com.smile.ssm.dao.TSysUserRoleDao;
import com.smile.ssm.service.TSysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户角色关系表 服务实现类
 *
 * @author Smile
 * @since 2021-11-24
 */
@Service
public class TSysUserRoleServiceImpl extends ServiceImpl<TSysUserRoleDao, TSysUserRolePO> implements TSysUserRoleService {

}
