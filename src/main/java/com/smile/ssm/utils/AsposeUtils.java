package com.smile.ssm.utils;

import com.aspose.cells.Workbook;
import com.aspose.slides.Presentation;
import com.aspose.words.*;
import com.aspose.words.net.System.Data.DataRow;
import com.aspose.words.net.System.Data.DataTable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.smile.ssm.utils.WatermarkUtils.insertWaterMarkMore;

/**
 * @description: 将doc, docx, txt文件转化为pdf文件工具类
 * @author: xpl
 * @date: 2019-12-09 19:02
 */
@Slf4j
public class AsposeUtils {
    /**
     * 获取 license
     */
    public static void getLicense() {
        try {
            String licenseStr =
                    "<License>\n" +
                            "  <Data>\n" +
                            "    <Products>\n" +
                            "      <Product>Aspose.Total for Java</Product>\n" +
                            "      <Product>Aspose.Words for Java</Product>\n" +
                            "    </Products>\n" +
                            "    <EditionType>Enterprise</EditionType>\n" +
                            "    <SubscriptionExpiry>20991231</SubscriptionExpiry>\n" +
                            "    <LicenseExpiry>20991231</LicenseExpiry>\n" +
                            "    <SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>\n" +
                            "  </Data>\n" +
                            "  <Signature>111</Signature>\n" +
                            "</License>";
            InputStream license = new ByteArrayInputStream(licenseStr.getBytes(StandardCharsets.UTF_8));
            License asposeLic = new License();
            asposeLic.setLicense(license);
        } catch (Exception e) {
            log.error("设置license失败", e);
        }

    }

    /**
     * 数组转换为文件返回
     *
     * @param byteArray  二进制数组
     * @param targetPath 目标文件路径
     * @return xx
     */
    public static File readBin2Image(byte[] byteArray, String targetPath) {
        InputStream in = new ByteArrayInputStream(byteArray);
        File file = new File(targetPath);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            int len;
            byte[] buf = new byte[1024];
            while ((len = in.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    /**
     * 将图片文件转换为pdf
     *
     * @param fileByte 数据流二进制数组
     * @param fileName 目标文件名
     * @throws Exception xx
     */
    public static void convertImageToPdf(byte[] fileByte, String fileName) throws Exception {
        getLicense();
        //将二进制数组放入文件中
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Document doc = new Document();
        DocumentBuilder builder = new DocumentBuilder(doc);
        // Load images from the disk using the approriate reader.
        // The file formats that can be loaded depends on the image readers available on the machine.
        File file = readBin2Image(fileByte, fileName);
        ImageInputStream iis = ImageIO.createImageInputStream(file);
        ImageReader reader = ImageIO.getImageReaders(iis).next();
        reader.setInput(iis, false);
        try {
            // Get the number of frames in the image.
            int framesCount = reader.getNumImages(true);

            // Loop through all frames.
            for (int frameIdx = 0; frameIdx < framesCount; frameIdx++) {
                // Insert a section break before each new page, in case of a multi-frame image.
                if (frameIdx != 0) {
                    builder.insertBreak(BreakType.SECTION_BREAK_NEW_PAGE);
                }

                // Select active frame.
                BufferedImage image = reader.read(frameIdx);

                // We want the size of the page to be the same as the size of the image.
                // Convert pixels to points to size the page to the actual image size.
                PageSetup ps = builder.getPageSetup();

                ps.setPageWidth(ConvertUtil.pixelToPoint(image.getWidth()));
                ps.setPageHeight(ConvertUtil.pixelToPoint(image.getHeight()));

                // Insert the image into the document and position it at the top left corner of the page.
                builder.insertImage(
                        image,
                        RelativeHorizontalPosition.PAGE,
                        0,
                        RelativeVerticalPosition.PAGE,
                        0,
                        ps.getPageWidth(),
                        ps.getPageHeight(),
                        WrapType.NONE);
            }
        } finally {
            iis.close();
            reader.dispose();
        }
        // Save the document to PDF.
        doc.save(bos, SaveFormat.PDF);
    }

    /**
     * 下载时给相应的word文件添加水印
     *
     * @param inputStream 数据类型
     * @param markStr     xx
     * @return xx
     */
    public static byte[] addWatermark(InputStream inputStream, String markStr) throws IOException {
        getLicense();
        byte[] bytes = new byte[0];
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Document doc = new Document(inputStream);
            insertWaterMarkMore(doc, markStr);
            FontSettings fontSettings = new FontSettings();
            FontFallbackSettings fontFallbackSettings = fontSettings.getFallbackSettings();
            fontFallbackSettings.loadNotoFallbackSettings();
            doc.setFontSettings(fontSettings);
            //全面支持DOC, DOCX, OCAML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(bos, SaveFormat.DOCX);
            bytes = bos.toByteArray();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
        return bytes;
    }


    /**
     * 将doc 文件转换为pdf 文件
     *
     * @param inputStream 数据输入流
     * @param fileName    目标文件名称
     * @return xx
     * @throws Exception xx
     */
    public static byte[] docTurnPdfByInputStream(InputStream inputStream, String fileName) throws Exception {
        getLicense();
        byte[] bytes = new byte[0];
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Document doc = new Document(inputStream);
            FontSettings fontSettings = new FontSettings();
            FontFallbackSettings fontFallbackSettings = fontSettings.getFallbackSettings();
            fontFallbackSettings.loadNotoFallbackSettings();
            doc.setFontSettings(fontSettings);
            //全面支持DOC, DOCX, OCAML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(bos, SaveFormat.PDF);
            bytes = bos.toByteArray();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
        return bytes;
    }

    /**
     * word文件转pdf导出
     *
     * @param resultByte xx
     */
    public static byte[] wordToPdfByByte(byte[] resultByte) {
        getLicense();
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(resultByte);
            Document doc = new Document(byteArrayInputStream);
            FontSettings fontSettings = new FontSettings();
            FontFallbackSettings fontFallbackSettings = fontSettings.getFallbackSettings();
            fontFallbackSettings.loadNotoFallbackSettings();
            doc.setFontSettings(fontSettings);
            doc.save(bos, SaveFormat.PDF);
            bytes = bos.toByteArray();
            bos.close();
            byteArrayInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * 将生成的文件上传并且返回文件路径
     *
     * @param resultByte xx
     */
    public static String wordToPdfByByteReturnFilePath(byte[] resultByte) {
        getLicense();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(resultByte);
            Document doc = new Document(byteArrayInputStream);
            FontSettings fontSettings = new FontSettings();
            FontFallbackSettings fontFallbackSettings = fontSettings.getFallbackSettings();
            fontFallbackSettings.loadNotoFallbackSettings();
            doc.setFontSettings(fontSettings);
            doc.save(bos, SaveFormat.PDF);
            bos.close();
            byteArrayInputStream.close();

        } catch (Exception e) {
            log.error("", e);
        }
        return null;
    }


    /**
     * 给word 文件添加页脚
     *
     * @param doc xx
     */
    public static void addHeaderFooter(Document doc) {
        //创建页脚 页码
        HeaderFooter footer = new HeaderFooter(doc, HeaderFooterType.FOOTER_PRIMARY);
        doc.getFirstSection().getHeadersFooters().add(footer);
        //页脚段落
        Paragraph paragraph = new Paragraph(doc);
        paragraph.getParagraphFormat().setAlignment(ParagraphAlignment.CENTER);
        Run run = new Run(doc);
        run.getFont().setName("宋体");
        //小5号字体
        run.getFont().setSize(9.0);
        paragraph.appendChild(run);
        try {
            //当前页码
            paragraph.appendField(FieldType.FIELD_PAGE, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        paragraph.appendChild(run);
        footer.appendChild(paragraph);
    }

    /**
     * excel 转换为 pdf
     *
     * @param inputStream xx
     * @param fileName    xx
     * @return xx
     * @throws Exception xx
     */
    public static String excelTurnPdf(InputStream inputStream, String fileName) throws Exception {
        getLicense();
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            Workbook workbook = new Workbook(inputStream);
            com.aspose.cells.PdfSaveOptions pdfSaveOptions = new com.aspose.cells.PdfSaveOptions();
            pdfSaveOptions.setOnePagePerSheet(true);
            pdfSaveOptions.setFontSubstitutionCharGranularity(true);
            workbook.save(bos, pdfSaveOptions);
            bos.toByteArray();
            return "fileId";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
        return null;
    }

    /**
     * ppt 转换为 pdf
     *
     * @param inputStream xx
     * @param fileName    xx
     * @return xx
     * @throws Exception xx
     */
    public static String pptxTurnPdf(InputStream inputStream, String fileName) throws Exception {
        try {
            Presentation presentationDocument = new Presentation(inputStream);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            presentationDocument.save(bos, com.aspose.slides.SaveFormat.Pdf);
            bos.toByteArray();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            inputStream.close();
        }
        return null;
    }


    /**
     * 调整buffering大小
     *
     * @param source  BufferedImage 原始image
     * @param targetW int  目标宽
     * @param targetH int  目标高
     * @param flag    boolean 是否同比例调整
     * @return BufferedImage  返回新image
     */
    public static BufferedImage resizeBufferedImage(BufferedImage source, int targetW, int targetH, boolean flag) {
        int type = source.getType();
        BufferedImage target;
        double sx = (double) targetW / source.getWidth();
        double sy = (double) targetH / source.getHeight();
        if (flag && sx > sy) {
            sx = sy;
            targetW = (int) (sx * source.getWidth());
        } else if (flag && sx <= sy) {
            sy = sx;
            targetH = (int) (sy * source.getHeight());
        }
        // handmade
        if (type == BufferedImage.TYPE_CUSTOM) {
            ColorModel cm = source.getColorModel();
            WritableRaster raster = cm.createCompatibleWritableRaster(targetW, targetH);
            boolean alphaPremultiplied = cm.isAlphaPremultiplied();
            target = new BufferedImage(cm, raster, alphaPremultiplied, null);
        } else {
            target = new BufferedImage(targetW, targetH, type);
        }
        Graphics2D g = target.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.drawRenderedImage(source, AffineTransform.getScaleInstance(sx, sy));
        g.dispose();
        return target;
    }


    /**
     * 填充 word 模板（object数据格式）
     *
     * @param modelWordByte word模版二进制文件
     * @param obj           要填充的数据
     * @return 组合数据之后的word二进制
     */
    public static byte[] fillWordDataByDomain(byte[] modelWordByte, Object obj) {
        getLicense();
        try {
            Class<?> aClass = obj.getClass();
            Field[] fields = aClass.getDeclaredFields();
            Map<String, Object> data = new HashMap<>(fields.length);
            for (Field field : fields) {
                PropertyDescriptor pd = new PropertyDescriptor(field.getName(), aClass);
                Method method = pd.getReadMethod();
                String key = field.getName();
                Object value = method.invoke(obj);
                data.put(key, value);
            }
            return fillWordDataByMap(modelWordByte, data);
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    /**
     * 填充 word 模板（当前只针对sae 的导出  map数据格式）
     *
     * @param file word二进制
     * @param data 要填充的数据
     * @return 组合数据之后的word二进制
     */
    public static byte[] fillWordDataByMap(byte[] file, Map<String, Object> data) throws Exception {
        byte[] ret;
        if (data == null || data.isEmpty()) {
            return null;
        }
        try (InputStream is = new ByteArrayInputStream(file);
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Document doc = new Document(is);
            DocumentBuilder builder = new DocumentBuilder(doc);
            Map<String, String> toData = new HashMap<>();
            for (Map.Entry<String, Object> en : data.entrySet()) {
                String key = en.getKey();
                Object value = en.getValue();

                if (value instanceof LocalDateTime) {
                    value = value.toString().replace("T", " ");
                }
                //如果属于图片
                if (value instanceof BufferedImage) {
                    builder.moveToMergeField(key);
                    builder.insertImage((BufferedImage) value);
                }
                if (null == value || "null".equals(value) || "".equals(value)) {
                    value = "";
                }
                toData.put(key, String.valueOf(value));
            }

            String[] fieldNames = new String[toData.size()];
            String[] values = new String[toData.size()];

            int i = 0;
            for (Map.Entry<String, String> entry : toData.entrySet()) {
                fieldNames[i] = entry.getKey();
                values[i] = entry.getValue();
                i++;
            }

            //合并数据
            doc.getMailMerge().execute(fieldNames, values);
            doc.save(out, SaveOptions.createSaveOptions(SaveFormat.DOCX));
            ret = out.toByteArray();
        }

        return ret;
    }


    /**
     * 封装 sae  list 数据到 word 模板中（word表格）
     *
     * @param list 数据
     * @return word表格数据DataTable
     */
    private static DataTable fillListData(List<Object> list) throws Exception {
        //需要对类型进行匹配(如果是一个包含属性的对象的话，如果包含String |long  )
        //创建DataTable,并绑定字段
        DataTable dataTable = new DataTable("saeDrugCombineList");
        if (!CollectionUtils.isEmpty(list)) {
            for (int j = 0; j < list.size(); j++) {
                //创建DataRow，封装该行数据
                DataRow dataRow = dataTable.newRow();
                Class<?> objClass = list.get(j).getClass();
                Field[] fields = objClass.getDeclaredFields();
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    //只处理第一次
                    if (dataTable.getColumns() == null || !dataTable.getColumns().contains(fields[i].getName())) {
                        Objects.requireNonNull(dataTable.getColumns()).add(fields[i].getName());
                    }
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), objClass);
                    Method method = pd.getReadMethod();
                    if (i == 0 && null != method.invoke(list.get(j))) {
                        dataRow.set(i, (j + 1) + ",  疾病：" + method.invoke(list.get(j)));
                    } else {
                        dataRow.set(i, method.invoke(list.get(j)));
                    }
                }
                dataTable.getRows().add(dataRow);
            }
        }
        return dataTable;
    }


    /**
     * 转换导出  试验药物接收登记表 数据
     *
     * @param outputStream 原始流
     * @return xx
     */
    public static byte[] changeWordAndToPdf(ByteArrayOutputStream outputStream) throws IOException {
        getLicense();
        byte[] bytes = new byte[0];
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputStream.toByteArray())) {
            Document doc = new Document(byteArrayInputStream);
            FontSettings fontSettings = new FontSettings();
            FontFallbackSettings fontFallbackSettings = fontSettings.getFallbackSettings();
            fontFallbackSettings.loadNotoFallbackSettings();
            doc.setFontSettings(fontSettings);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(bos, SaveFormat.DOCX);
            bytes = bos.toByteArray();
            byteArrayInputStream.close();
            bos.close();
            return bytes;
        } catch (Exception e) {
            log.error("文件流出现异常", e);
        }
        return bytes;
    }


    /**
     * 根据数据每次填充特殊的两行添加到原始模板中
     *
     * @param doc 文档
     */
    private static void createSpecialModule(Document doc) {
        //这里进行特殊处理   获取到第二个 table 中的数据进行一行行的插入
        Table table = (Table) doc.getChild(NodeType.TABLE, 1, true);
        String[] strings = {};
        Row row = createRow(11, strings, doc);
        table.appendChild(row);
        String[] strings1 = {};
        Row row1 = createRow(11, strings1, doc);
        table.appendChild(row1);
        mergeCells(row1.getFirstCell(), row1.getLastCell());
    }

    /**
     * Merges the range of cells found between the two specified cells both
     * horizontally and vertically. Can span over multiple rows.
     *
     * @param startCell 开始单元格位置
     * @param endCell   结束单元格位置
     */
    public static void mergeCells(Cell startCell, Cell endCell) {
        Table parentTable = startCell.getParentRow().getParentTable();
        // Find the row and cell indices for the start and end cell.
        Point startCellPos = new Point(startCell.getParentRow().indexOf(startCell), parentTable.indexOf(startCell.getParentRow()));
        Point endCellPos = new Point(endCell.getParentRow().indexOf(endCell), parentTable.indexOf(endCell.getParentRow()));
        // Create the range of cells to be merged based off these indices. Inverse each index if the end cell if before the start cell.
        Rectangle mergeRange = new Rectangle(Math.min(startCellPos.x, endCellPos.x), Math.min(startCellPos.y, endCellPos.y), Math.abs(endCellPos.x - startCellPos.x) + 1,
                Math.abs(endCellPos.y - startCellPos.y) + 1);
        for (Row row : parentTable.getRows()) {
            for (Cell cell : row.getCells()) {
                Point currentPos = new Point(row.indexOf(cell), parentTable.indexOf(row));
                // Check if the current cell is inside our merge range then merge it.
                if (mergeRange.contains(currentPos)) {
                    if (currentPos.x == mergeRange.x) {
                        cell.getCellFormat().setHorizontalMerge(CellMerge.FIRST);
                    } else {
                        cell.getCellFormat().setHorizontalMerge(CellMerge.PREVIOUS);
                    }

                    if (currentPos.y == mergeRange.y) {
                        cell.getCellFormat().setVerticalMerge(CellMerge.FIRST);
                    } else {
                        cell.getCellFormat().setVerticalMerge(CellMerge.PREVIOUS);
                    }
                }
            }
        }
    }

    /**
     * 创建行
     *
     * @param columnCount  行数量
     * @param columnValues 行里面每一列的值
     * @param doc          原始文档
     * @return 返回行
     */
    public static Row createRow(int columnCount, String[] columnValues, Document doc) {
        Row r2 = new Row(doc);
        for (int i = 0; i < columnCount; i++) {
            Cell cell;
            if (columnValues.length > i) {
                cell = createCell(StringUtils.isEmpty(columnValues[i]) ? "" : columnValues[i], doc);
            } else {
                cell = createCell("", doc);
            }
            r2.getCells().add(cell);

        }
        return r2;
    }

    /**
     * 创建列
     *
     * @param value 列值
     * @param doc   原始文档
     * @return 返回列
     */
    public static Cell createCell(String value, Document doc) {
        Cell c1 = new Cell(doc);
        Paragraph p = new Paragraph(doc);
        p.appendChild(new Run(doc, value));
        c1.appendChild(p);
        return c1;
    }

}
