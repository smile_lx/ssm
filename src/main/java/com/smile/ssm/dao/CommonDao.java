package com.smile.ssm.dao;

import com.smile.ssm.common.CommonDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 系统公用表 Mapper 接口
 *
 * @author Smile
 * @since 2022-03-24
 */
public interface CommonDao extends BaseMapper<CommonDO> {

}
