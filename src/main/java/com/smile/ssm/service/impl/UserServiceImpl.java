package com.smile.ssm.service.impl;

import com.smile.ssm.entity.UserDO;
import com.smile.ssm.dao.UserDao;
import com.smile.ssm.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统用户表 服务实现类
 *
 * @author Smile
 * @since 2022-03-24
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserDO> implements UserService {

}
