package com.smile.ssm.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * 小票备注信息表 API控制器
 *
 * @author Smile
 * @since 2021-12-10
 */
@RestController
@RequestMapping("/ticket-remarks-po")
public class TicketRemarksApi {

}
