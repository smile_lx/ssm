//package com.smile.ssm.timer;
//
//import com.smile.ssm.entity.SysUserPO;
//import com.smile.ssm.service.SysUserService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.List;
//
///**
// * 定时任务测试类
// *
// * @author Smile
// */
//@Component
//@Slf4j
//public class ScheduleTimeTest {
//
//    @Resource
//    private SysUserService service;
//
//    @Async("asyncScheduleExecutor")
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void test1() {
//        System.out.println("定时任务1开始");
//        List<SysUserPO> list = service.list();
//        System.out.println(list);
//
//        System.out.println("定时任务1结束");
//    }
//
//    @Async("asyncScheduleExecutor")
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void test2() {
//        System.out.println("定时任务2开始");
//        List<SysUserPO> list = service.list();
//        System.out.println(list);
//        System.out.println("定时任务2结束");
//    }
//
//    @Async("asyncScheduleExecutor")
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void test3() {
//        System.out.println("定时任务3");
//        List<SysUserPO> list = service.list();
//        System.out.println(list);
//        System.out.println("定时任务3结束");
//    }
//
//    @Async("asyncScheduleExecutor")
//    @Scheduled(cron = "0/10 * * * * ?")
//    public void test4() {
//        System.out.println("定时任务4");
//        List<SysUserPO> list = service.list();
//        System.out.println(list);
//        System.out.println("定时任务4结束");
//    }
//
//
//}
