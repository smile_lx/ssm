package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.smile.ssm.CommonPO;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 系统日志表 实体
 *
 * @author Smile
 * @since 2021-11-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_log")
public class SysLogPO extends CommonPO {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 调用方法名称
     */
    @TableField("method")
    private String method;

    /**
     * 业务名称
     */
    @TableField("business_name")
    private String businessName;

    /**
     * 错误表述
     */
    @TableField("err_description")
    private String errDescription;

    /**
     * 调用ip
     */
    @TableField("ip")
    private String ip;

    /**
     * 逻辑删除标志（0：未删除，1：已删除）
     */
    @TableField("deleted")
    private Boolean deleted;

    /**
     * 登录默认机构
     */
    @TableField("default_org_id")
    private Long defaultOrgId;

    /**
     * 登录默认角色
     */
    @TableField("default_role_id")
    private Long defaultRoleId;

    /**
     * 创建人
     */
    @TableField("create_by")
    private Long createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("update_by")
    private Long updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private LocalDateTime deleteTime;


}
