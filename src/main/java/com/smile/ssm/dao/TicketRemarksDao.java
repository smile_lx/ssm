package com.smile.ssm.dao;

import com.smile.ssm.entity.TicketRemarksPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 小票备注信息表 Mapper 接口
 *
 * @author Smile
 * @since 2021-12-10
 */
public interface TicketRemarksDao extends BaseMapper<TicketRemarksPO> {

}
