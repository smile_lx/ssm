



CREATE TABLE `common` (
                          `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据库主键自增id',
                          `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标志位',
                          `create_time` datetime NOT NULL COMMENT '数据创建时间',
                          `delete_time` datetime DEFAULT NULL COMMENT '数据删除时间',
                          `update_time` datetime NOT NULL COMMENT '数据修改时间',
                          `create_by` bigint(12) DEFAULT NULL COMMENT '新增人员id',
                          `update_by` bigint(12) DEFAULT NULL COMMENT '修改人员id',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='建表标准语句';

CREATE TABLE `user` (
                          `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据库主键自增id',


                          `user_name` varchar(100) NOT NULL COMMENT '用户名',
                          `password` varchar(100) NOT NULL COMMENT '密码',
                          `real_name` varchar(50) NOT NULL COMMENT '用户真实姓名',
                          `mobile` varchar(20) DEFAULT NULL COMMENT '移动电话',
                          `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
                          `status` tinyint(1) NOT NULL COMMENT '停启用状态（0：停用，1：启用）',
                          `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
                          `last_login_ip` varchar(50) DEFAULT NULL COMMENT '最后登录IP',



                          `deleted` int(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除标志位',
                          `create_time` datetime NOT NULL COMMENT '数据创建时间',
                          `delete_time` datetime DEFAULT NULL COMMENT '数据删除时间',
                          `update_time` datetime NOT NULL COMMENT '数据修改时间',
                          `create_by` bigint(12) DEFAULT NULL COMMENT '新增人员id',
                          `update_by` bigint(12) DEFAULT NULL COMMENT '修改人员id',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统用户表';