//package com.smile.ssm.config;
//
//import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @ClassName MybatisPlusConfig
// * @Description: TODO
// * @Author Smile
// * @Date 2021/5/10
// * @Version V1.0
// **/
//@Configuration
//public class MybatisPlusConfig {
//    /**
//     * 分页插件
//     */
//    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        return new PaginationInterceptor();
//    }
//
//}
