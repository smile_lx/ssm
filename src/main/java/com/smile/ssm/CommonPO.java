package com.smile.ssm;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Smile
 * @version v1.0
 * @description 系统全局公共父类
 * @time 2021/11/24 13:34
 */
@Data
public class CommonPO implements Serializable {
    @TableId
    private Long id;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;


}
