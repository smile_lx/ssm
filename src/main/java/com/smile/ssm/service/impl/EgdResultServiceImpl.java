package com.smile.ssm.service.impl;

import com.smile.ssm.entity.EgdResultPO;
import com.smile.ssm.dao.EgdResultDao;
import com.smile.ssm.service.EgdResultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 心电图检查结果信息表 服务实现类
 *
 * @author Smile
 * @since 2021-12-21
 */
@Service
public class EgdResultServiceImpl extends ServiceImpl<EgdResultDao, EgdResultPO> implements EgdResultService {

}
