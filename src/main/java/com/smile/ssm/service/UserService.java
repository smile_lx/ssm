package com.smile.ssm.service;

import com.smile.ssm.entity.UserDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 系统用户表 服务类
 *
 * @author Smile
 * @since 2022-03-24
 */
public interface UserService extends IService<UserDO> {

}
