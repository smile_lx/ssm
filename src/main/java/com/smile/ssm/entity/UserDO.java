package com.smile.ssm.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * 系统用户表 实体
 *
 * @author Smile
 * @since 2022-03-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user")
public class UserDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库主键自增id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 用户真实姓名
     */
    @TableField("real_name")
    private String realName;

    /**
     * 移动电话
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 电子邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 停启用状态（0：停用，1：启用）
     */
    @TableField("status")
    private Boolean status;

    /**
     * 最后登录时间
     */
    @TableField("last_login_time")
    private LocalDateTime lastLoginTime;

    /**
     * 最后登录IP
     */
    @TableField("last_login_ip")
    private String lastLoginIp;

    /**
     * 逻辑删除标志位
     */
    @TableField("deleted")
    private Integer deleted;

    /**
     * 数据创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 数据删除时间
     */
    @TableField("delete_time")
    private LocalDateTime deleteTime;

    /**
     * 数据修改时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;

    /**
     * 新增人员id
     */
    @TableField("create_by")
    private Long createBy;

    /**
     * 修改人员id
     */
    @TableField("update_by")
    private Long updateBy;


}
