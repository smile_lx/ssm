package com.smile.ssm.enums;

/**
 * 标准demo
 *
 * @author Smile
 */

public enum DemoEnum {
    /**
     * 类型
     */
    TYPE1(1, "demo1"),
    TYPE2(2, "demo2");
    /**
     * key
     */
    private int key;
    /**
     * value
     */
    private String value;

    DemoEnum(int key, String value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

}
