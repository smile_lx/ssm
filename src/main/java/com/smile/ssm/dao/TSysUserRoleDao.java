package com.smile.ssm.dao;

import com.smile.ssm.entity.TSysUserRolePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 用户角色关系表 Mapper 接口
 *
 * @author Smile
 * @since 2021-11-24
 */
public interface TSysUserRoleDao extends BaseMapper<TSysUserRolePO> {

}
