package com.smile.ssm.service;

import com.smile.ssm.entity.TicketRemarksPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 小票备注信息表 服务类
 *
 * @author Smile
 * @since 2021-12-10
 */
public interface TicketRemarksService extends IService<TicketRemarksPO> {

}
