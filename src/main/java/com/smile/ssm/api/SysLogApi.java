package com.smile.ssm.api;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * 系统日志表 API控制器
 *
 * @author Smile
 * @since 2021-11-29
 */
@RestController
@RequestMapping("/sys-log-po")
public class SysLogApi {

}
