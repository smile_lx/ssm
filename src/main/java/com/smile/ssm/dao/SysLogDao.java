package com.smile.ssm.dao;

import com.smile.ssm.entity.SysLogPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 系统日志表 Mapper 接口
 *
 * @author Smile
 * @since 2021-11-29
 */
public interface SysLogDao extends BaseMapper<SysLogPO> {

}
