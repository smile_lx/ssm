package com.smile.ssm.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName HttpResponseUtil
 * @Description: 将处理好的文件流转发给前端
 * @Author Smile
 * @Date 2020/9/22
 * @Version V1.0
 **/
@Component
@Slf4j
public class HttpResponseUtil {
    /**
     * 用于excel 导出 转换给前端处理
     *
     * @param response xx
     * @param newFile  xx
     * @throws IOException xx
     */
    public static void toWep(HttpServletResponse response, HttpServletRequest request, File newFile) throws IOException {
        String fileName = newFile.getName();
        InputStream fis = new BufferedInputStream(new FileInputStream(newFile));
        byte[] buffer = new byte[fis.available()];
        fis.read(buffer);
        fis.close();
        // 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
        String userAgent = request.getHeader("user-agent").toLowerCase();
        userAgent = userAgent.toLowerCase();
        if (userAgent.contains("msie") || userAgent.contains("trident") || userAgent.contains("like gecko")) {
            // win10 ie edge 浏览器 和其他系统的ie
            fileName = URLEncoder.encode(fileName, "UTF-8");
        } else {
            // fe
            fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
        }
        // 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
        response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
        response.addHeader("Content-Length", "" + newFile.length());
        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        response.setContentType("application/octet-stream");
        os.write(buffer);
        os.flush();
        os.close();
    }

    /**
     * 用于word 导出 转换给前端处理
     *
     * @param response   xx
     * @param resultByte xx
     * @throws IOException xx
     */
    public static void toWep(HttpServletResponse response, HttpServletRequest request, byte[] resultByte, String fileName) throws IOException {
        // 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
        String userAgent = request.getHeader("user-agent").toLowerCase();
        userAgent = userAgent.toLowerCase();
        if (userAgent.contains("msie") || userAgent.contains("trident") || userAgent.contains("like gecko")) {
            // win10 ie edge 浏览器 和其他系统的ie
            fileName = URLEncoder.encode(fileName, "UTF-8");
        } else {
            // fe
            fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
        }
        // 先去掉文件名称中的空格,然后转换编码格式为utf-8,保证不出现乱码,这个文件名称用于浏览器的下载框中自动显示的文件名
        response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.addHeader("Content-Length", "" + resultByte.length + "@fileName=" + new String(fileName.replaceAll(" ", "").getBytes(StandardCharsets.UTF_8)));
        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        response.setContentType("application/octet-stream");
        os.write(resultByte);
        os.flush();
        os.close();
    }

}
