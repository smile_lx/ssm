package com.smile.ssm.cons;

/**
 * @author Smile
 * @version v1.0
 * @description http相关的常量
 * @time 2021/11/25 14:39
 */
public class HttpCons {
    /**
     * http请求类型常量 OPTIONS
     */
    public final static String OPTIONS = "OPTIONS";

    /**
     * 请求成功
     */
    public final static String SUCCESS = "200";

    /**
     * 请求失败
     */
    public final static String FAIL = "500";

    /**
     * 请求没有权限
     */
    public final static String NO_AUTHENTICATE  = "403";



}
