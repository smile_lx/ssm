package com.smile.ssm.dao;

import com.smile.ssm.entity.EgdResultPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 * 心电图检查结果信息表 Mapper 接口
 *
 * @author Smile
 * @since 2021-12-21
 */
public interface EgdResultDao extends BaseMapper<EgdResultPO> {

}
