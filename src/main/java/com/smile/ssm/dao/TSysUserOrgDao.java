package com.smile.ssm.dao;

import com.smile.ssm.entity.TSysUserOrgPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *
 *  Mapper 接口
 *
 * @author Smile
 * @since 2021-11-24
 */
public interface TSysUserOrgDao extends BaseMapper<TSysUserOrgPO> {

}
