package com.smile.ssm.service;

import com.smile.ssm.common.CommonDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 系统公用表 服务类
 *
 * @author Smile
 * @since 2022-03-24
 */
public interface CommonService extends IService<CommonDO> {

}
