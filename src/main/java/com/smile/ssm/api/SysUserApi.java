package com.smile.ssm.api;


import com.smile.ssm.aop.annotation.SysLog;
import com.smile.ssm.common.R;
import com.smile.ssm.service.SysUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户表 API控制器
 *
 * @author Smile
 * @since 2021-11-24
 */
@RestController
@RequestMapping("/sys-user-po")
public class SysUserApi {
    @Resource
    private SysUserService service;

    @GetMapping("/list")
    public R list() {
        return R.success(service.list());
    }

    @GetMapping("/detail")
    @SysLog(value = "获取用户详细信息")
    public R list(@RequestParam() String uid)
    {
        return R.success(service.getById(uid));
    }

    @GetMapping("/detailError")
    @SysLog(value = "错误信息处理")
    public R listError(@RequestParam() String uid)
    {
        System.out.println(1/0);
        return R.success(service.getById(uid));
    }


}
