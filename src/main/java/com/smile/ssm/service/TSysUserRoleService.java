package com.smile.ssm.service;

import com.smile.ssm.entity.TSysUserRolePO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * 用户角色关系表 服务类
 *
 * @author Smile
 * @since 2021-11-24
 */
public interface TSysUserRoleService extends IService<TSysUserRolePO> {

}
