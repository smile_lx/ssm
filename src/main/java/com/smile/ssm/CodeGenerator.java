package com.smile.ssm;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Smile
 * @version v1.0
 * @description 代码生成器
 * @time 2021/11/24 10:49
 */
public class CodeGenerator {
    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入").append(tip).append("：");
        System.out.println(help);
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }


    public static void main(String[] args) {
        CodeGenerator.selfSuccess();
    }

    public static void selfSuccess() {
        String projectPath = System.getProperty("user.dir");
        // 实例化代码生成器类
        AutoGenerator mpg = new AutoGenerator();

        //全局地址配置 以及部分规则配置
        mpg.setGlobalConfig(CodeGenerator.globalConfig(projectPath));

        //数据源配置
        mpg.setDataSource(CodeGenerator.dataSourceConfig());

        PackageConfig pc = CodeGenerator.packageConfig();
        // 包配置
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        templateConfig.setEntity("templates/entity.java");
        templateConfig.setMapper("templates/mapper.java");
        templateConfig.setController("templates/controller.java");
        templateConfig.setServiceImpl("templates/service.java");
        templateConfig.setServiceImpl("templates/serviceImpl.java");

        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        //策略
        StrategyConfig strategy = CodeGenerator.strategyConfig();
        mpg.setStrategy(strategy);

        //ftl的需要这个步骤
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        //执行
        mpg.execute();
    }

    /**
     * 自动代码生成 全局 GlobalConfig 配置
     *
     * @return GlobalConfig
     */
    public static GlobalConfig globalConfig(String projectPath) {
        GlobalConfig gc = new GlobalConfig();
        //项目根路径

        //生成文件的输出目录
        //默认值：D 盘根目录
        gc.setOutputDir(projectPath + "/src/main/java");

        //作者
        gc.setAuthor("Smile");

        //是否覆盖已有文件
        //默认值：false
        gc.setFileOverride(false);

        //是否打开输出目录
        //默认值：true
        gc.setOpen(false);

        //实体命名方式
        //默认值：null 例如：%sEntity 生成 UserEntity
        gc.setEntityName("%sDO");

        //mapper 命名方式
        //默认值：null 例如：%sDao 生成 UserDao
        gc.setMapperName("%sDao");

        //Mapper xml 命名方式
        //默认值：null 例如：%sDao 生成 UserDao.xml
        gc.setXmlName("%sDao");

        //service 命名方式
        //默认值：null 例如：%sBusiness 生成 UserBusiness
        gc.setServiceName("%sService");

        //service impl 命名方式
        //默认值：null 例如：%sBusinessImpl 生成 UserBusinessImpl
        gc.setServiceImplName("%sServiceImpl");

        //controller 命名方式
        //默认值：null 例如：%sAction 生成 UserAction
        gc.setControllerName("%sApi");
        return gc;
    }

    /**
     * 自动生成数据源配置
     *
     * @return DataSourceConfig
     */
    public static DataSourceConfig dataSourceConfig() {
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://192.168.99.201:3306/standard?useUnicode=true&useSSL=false&characterEncoding=utf8");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("Woaicy66");
        return dsc;
    }

    /**
     * 报包属性配置 包名啥的
     *
     * @return xx
     */
    public static PackageConfig packageConfig() {
        PackageConfig pc = new PackageConfig();

        //父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
        pc.setParent("com.smile.ssm");

//        //父包模块名 父包下面的模块名
//        pc.setModuleName(scanner("模块名"));

        //Entity包名
        pc.setEntity("entity");

        //Mapper包名
        pc.setMapper("dao");

        //xml包名
        pc.setXml("mapper");

        //Controller包名
        pc.setController("api");

        return pc;
    }

    /**
     * 策略配置
     *
     * @return 返回配置的策略
     */
    public static StrategyConfig strategyConfig() {
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setEntityBooleanColumnRemoveIsPrefix(true);
        strategyConfig.setEntityTableFieldAnnotationEnable(true);
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.setControllerMappingHyphenStyle(true);
//        // 表名前辍
//        strategyConfig.setTablePrefix("s_");
//        // 自定义继承的Entity类全称，带包名
//        strategyConfig.setSuperEntityClass(Common.class);
        strategyConfig.setEntityTableFieldAnnotationEnable(true);

        return strategyConfig;
    }
}
